# Documentation

This is the snippet for a "Decision Tree Algorithm implementation" developed in python.

# Environment Setup

- Setup Python3 & pip in your system
- Clone the repo
- Run the following commands for the required dependencies
```
pip install pandas sklearn pydotplus matplotlib
apt-get install graphviz
```

# Run Program

Go to the root folder & run following command.

```
python3 program.py

```

# Output
![](mydecisiontree.png)